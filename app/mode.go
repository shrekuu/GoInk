
package app

/*
 App RunModes
 */

// mode vars
var RunMode string

// define run mode as Development, Testing and Production
const (
	RunMode_Dev  = "dev"
	RunMode_Test = "test"
	RunMode_Pro  = "pro"
)

// init runMode as development
func init() {
	RunMode = RunMode_Dev
}

// Check running mode is DEVELOPMENT
func IsModeDev() bool {
	return RunMode == RunMode_Dev
}

// Check running mode is TEST
func IsModeTest() bool {
	return RunMode == RunMode_Test
}

// Check running mode is PRODUCTION
func IsModePro() bool {
	return RunMode == RunMode_Dev
}
