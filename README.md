## HxGo Framework

HxGo is a simple Go Web Framework and very easy to use. Its' idea is from my php experience. HxGo is followed MVC structure and you can code as other mvc frameworks.

#### Features

* RESTful and auto-map router support
* MVC architecture support
* Multi-Config and multi-logger
* Simple template wrapper
* Global http context operation
* Simple database operation, sql builder and table wrapper

#### Documentation

In [wikis](http://git.oschina.net/fuxiaohei/hxgo/wikis/home)

### Use case

- FuXiaoHei.Go project [fuxiaohei.go](http://git.oschina.net/fuxiaohei/fuxiaohei-go)