package cache

import "sync"

// MemoryCache struct
type MemoryCache struct {
	locker *sync.Mutex
	cachedData map[string]interface {}
}

// create new MemoryCache struct
func NewMemoryCache() *MemoryCache {
	c := &MemoryCache{}
	c.locker = new(sync.Mutex)
	c.cachedData = map[string]interface {}{}
	return c
}

// put cache data into
func (this *MemoryCache) Set(key string, value interface {}, expire int) {
	this.locker.Lock()
	defer this.locker.Unlock()
	this.cachedData[key] = value
}

// get cache data
func (this *MemoryCache) Get(key string) interface {} {
	this.locker.Lock()
	defer this.locker.Unlock()
	return this.cachedData[key]
}

// delete cache data
func (this *MemoryCache) Del(key string) {
	this.locker.Lock()
	defer this.locker.Unlock()
	if _, ok := this.cachedData[key]; ok {
		delete(this.cachedData, key)
	}
}

// check if cache data exists
func (this *MemoryCache) Has(key string) bool {
	this.locker.Lock()
	defer this.locker.Unlock()
	if _, ok := this.cachedData[key]; !ok {
		return false
	}
	return true
}

// clear all cache data
func (this *MemoryCache) Clear() {
	this.locker.Lock()
	defer this.locker.Unlock()
	this.cachedData = make(map[string]interface {})
}

// gc all cache data, invalid func in MemoryCache
func (this *MemoryCache) Gc() {

}


